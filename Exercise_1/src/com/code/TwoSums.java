package com.code;

import java.util.Scanner;

public class TwoSums {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        System.out.println("Enter the target");
        int target = s.nextInt();
        System.out.println("Enter the size of array");
        int length = s.nextInt();
        System.out.println("Enter the values ");
        int[] nums = new int [length];
        for(int i=0;i<length;i++){
            nums[i] = s.nextInt();
        }
        System.out.println("No of Islands is "+ twoSum(nums,target));

    }
    private static int[] twoSum(int[] nums, int target) {

        int[] result = new int[2];
        int arrayLength = nums.length;

        for(int i=0; i<arrayLength-1; i++) {
            for(int j=i+1; j<arrayLength; j++) {
                if(nums[i] + nums[j] == target){
                    result[0] = i;
                    result[1] = j;
                    return result;
                }
            }
        }
        return result;
    }
}
