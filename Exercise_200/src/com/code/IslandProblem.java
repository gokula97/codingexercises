package com.code;

import java.util.Scanner;

public class IslandProblem {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        System.out.println("Enter value of i");
        int row = s.nextInt();
        System.out.println("Enter value of j");
        int col = s.nextInt();
        System.out.println("Enter the matrix values ");

        int[][] grid = new int [row][col];
        for(int i=0;i<row;i++){
            for(int j=0;j<col;j++){
                grid[i][j] = s.nextInt();
            }
        }
        System.out.println("No of Islands is "+ countNoOfIslands(grid));
    }

    private static int countNoOfIslands(int[][] grid) {
        int noOfIslands=0;

        if(grid==null || grid.length==0||grid[0].length==0){
            return 0;
        }
        int row = grid.length;
        int col = grid[0].length;

        for(int i=0; i<row; i++){
            for(int j=0; j<col; j++){
                if(grid[i][j]==1){
                    noOfIslands++;
                    deepCheck(grid, i, j);
                }
            }
        }
        return noOfIslands;
    }

    private static void deepCheck(int[][] grid, int i, int j){
        int row=grid.length;
        int col=grid[0].length;

        if( i<0 || j<0 || i>=row || j>=col || grid[i][j]!=1 ){
            return;
        }
        grid[i][j]=-1;
        deepCheck(grid, i-1, j);
        deepCheck(grid, i+1, j);
        deepCheck(grid, i, j-1);
        deepCheck(grid, i, j+1);
    }
}
